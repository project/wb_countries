DESCRIPTION
------------
This module provides World Bank countries taxonomy list and regions taxonomy list.

REQUIREMENTS
------------
Drupal 8.x || Drupal 9.x

INSTALLATION
------------
1.  Place wb_countries module into your modules directory.
    This is normally the "modules" directory.

2.  Go to admin/modules. Enable modules.
    The WB Countries and WB Regions can be found in the Taxonomy section.

REFERENCES
----------
http://api.worldbank.org/v2/country?format=json&per_page=500
